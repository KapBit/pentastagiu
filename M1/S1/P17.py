s = str(raw_input("Enter a string\n>>> "))
S = ""
l = list(s)
diff = ord("a") - ord("A") #I know it's 32, just making sure

for letter in l:

    if ord(letter) >= 97:
        letter = chr(ord(letter)-diff)

    else:
        letter = chr(ord(letter)+diff)
        
    S += letter
    
print(S)
