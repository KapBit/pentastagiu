import sys

def p1(*args):
    S = 0
    for val in sys.argv[1:]:
        S += float(val)
    return S

print(p1())
