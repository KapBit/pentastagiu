import facultati as f

class operations:
	@staticmethod
	def add_candidate(faculty, candidate):
		f.faculty.student_list.append(candidate)	
	@staticmethod
	def remove_candidate(faculty, candidate):
		f.faculty.student_list.remove(candidate)
	@staticmethod
	def list_candidates():
		print(f.faculty.student_list)
	
	@staticmethod
	def list_results(faculty):
		for student in faculty.student_list:
			print(student.name,":",student.grade)

	@staticmethod
	def add_university(name):
		f.university_list.append(f.university(name))

	@staticmethod
	def list_universities():
		print(f.university_list)

	@staticmethod
	def add_faculty(university, name):
		university.faculty_list.append(faculty(name))

	@staticmethod
	def list_faculties(name):
		print(f.name.faculty_list)



class gui:	
	def print_commands():
		print("stuff")
	
	def	gui_list_uni():
		pass

	def	gui_add_uni():
		pass

	def gui_list_fac():
		pass

	def gui_add_fac():
		pass

	def gui_list_candidates():
		pass

	def gui_add_candidate():
		pass

	def gui_remove_candidate():
		pass

	option_dict = {
					'0':print_commands,
					'1':gui_list_uni,
					'2':gui_add_uni,
					'3':gui_list_fac,
					'4':gui_add_fac,
					'5':gui_list_candidates,
					'6':gui_add_candidate,
					'7':gui_remove_candidate
					}
	def start(self):
		choice = str(input("What would you like to do? Enter 0 for a list of commands\n>>>"))
		self.option_dict[choice](self)
		start()

