n = int(input("Enter n:\n>>> "))
P = 1

for num in range(1,n+1):
    P *= num

print("P = {}").format(P)
