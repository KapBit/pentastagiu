import sys

def p2(val):
    if val > 0:
        return val + p2(val - 1)
    else:
        return 0

print(p2(int(sys.argv[1])))
