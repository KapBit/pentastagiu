import sys

def p4(n, i = 0, maxn = 0):
  if not maxn:
    n = maxn
    print("*")
    return(n, i, maxn)
  if i != maxn:
    print("* "*i)
    i += 1
  else:
    print("* "*maxn)

v = input()
v = int(v)
p4(v,v)
