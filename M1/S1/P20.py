import sys

P = 1

for num in sys.argv[1:]: #start iterating after argv[0] which is prog name
    P *= int(num)
print("P = {}".format(P))
