def p6(s, l = []):
    
    if len(s):
        letter = s[0]
        if ord(letter) >= 97:
            letter = chr(ord(letter)-32)

        else:
            letter = chr(ord(letter)+32)
        
        l.append(letter)
        s = s[1:]
        p6(s, l)

    return s, l

a , b = p6("SwapCase")
print(b)
