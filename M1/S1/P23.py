l = raw_input("Enter a list\n>>> ").split(",")
n = int(raw_input("Enter n\n>>> "))

ll = [] #list of lists
var = 0
div = len(l) / n
maxes = []

while div:
    ll.append(l[var:(var + n)])
    div -= 1
    var += n

for subl in ll:
    maxel = -999
    for elem in subl:
        if elem > maxel:
            maxel = elem
    maxes.append(maxel)
    
print(maxes)
