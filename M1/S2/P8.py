def comp(num1, num2):
    if num1 > num2:
        return 1
    elif num1 < num2:
        return -1
    else:
        return 0

def p8(f, l):
    for i in range(len(l)):
        for j in range(len(l)-1-i):
            if comp(l[j],l[j+1]):
                l[j], l[j+1] = l[j+1], l[j]
    return l

stuff = [2, 5, 8, 3, 4, 1]

nstuff = p8(comp, stuff)
print(nstuff)
    
