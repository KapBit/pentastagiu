##CMMDC##

r = 1 # else while loop won't start
a = input("Enter a\n>>> ")
b = input("Enter b\n>>> ")

x, y = a, b


if b > a:
    a, b = b, a # Python's pretty great for allowing this

while r != 0:
    r = a % b
    a = b
    b = r

if __name__ == "__main__": # Preparing to cheat
    print("CMMDC = {}".format(a))

