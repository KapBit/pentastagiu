import sys

def p2(*args):
    maxv = -9999
    minv = 9999
    for val in sys.argv[1:]:
        val = float(val)
        if val > maxv:
            maxv = val
        if val < minv:
            minv = val
    return (minv, maxv)

print(p2())
