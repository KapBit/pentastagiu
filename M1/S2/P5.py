from collections import deque

def p5(aString):
    dq = deque()

    for ch in aString:
        dq.append(ch)

    ispal = True

    while len(dq) > 1 and ispal:
        first = dq.pop()
        last = dq.popleft()
    if first != last:
        ispal = False
        return ispal
    return(ispal)
    
print(p5("car"))
print(p5("racecar"))
