import pickle
import json
import re

class pbook:

    #plist = {"Urgenta": 112, "Serviciu Client": 433, "Credit": 333}
    plist = []
    def addc(self, name, num):
        #name = pers(name,num)
        #self.plist.append(name)
        self.plist.append(pers(name,num))  
    def findc(self, sname):
        for i in range(len(self.plist)):
            if self.plist[i].name == sname:
                print(self.plist[i].name, "has the number", self.plist[i].num)
                return
        print("Name is not in list")
   
    def editn(self, name, newval):
        for i in range(len(self.plist)):
            if self.plist[i].name == name:        
                self.plist[i].name = newval

    def editnu(self, name, newval):
        for i in range(len(self.plist)):
            if self.plist[i].name == name:
                self.plist[i].num = newval
    
    def editmail(self, name, newval):
        for i in range(len(self.plist)):
            if self.plist[i].name == name:
                self.plist[i].mail = newval

    def delc(self, dname):
        for i in (self.plist):
            if i.name == dname:
                self.plist.remove(i)
                
        
    def list(self):
        for i in self.plist:
            print(i.name,i.num)
        
class regex:
    def checknum(num):
        if regexon:
            return re.match(r"\d{7}", num)
        else:
            pass


    def checkmail(mail):
        if regexon:        
            return re.match(r".*@.*\..*",mail)
        else:
            pass
            
class GUI:
    sawhelp = 0
    def g0(self):
        if self.sawhelp:
            comm = input("What would you like to do? Enter 1 for a list of commands\n>>> ")
        else:
            sawhelp = 1
            comm = int(input("What would you like to do?\n>>> "))

        if comm == 1: self.g1()
        elif comm == 2: self.g2()
        elif comm == 3: self.g3()
        elif comm == 4: self.g4()
        elif comm == 5: self.g5()
        elif comm == 6: self.g6()
        elif comm == 7: self.g7()
        elif comm == 8: self.g8()
        else: print("Invalid command")
        
        self.g0()



    def g1(self):
        print("""
1: Help
2: View contacts
3: Search contacts
4: Add contact
5: Delete contact
6: Edit contact
7: Save and/or exit
8: Edit/Add mail
 """)
    
            
    def g2(self):
        p.list()
    
    def g3(self):
        name = str(input("Who would you like to view?\n>>> "))
        p.findc(name)
    def g4(self):
        name = str(input("Enter new contact name\n>>> "))
        num = input("Enter new contact number\n>>> ")
        p.addc(name,num)

    def g5(self):
        deltarget = str(input("Who do you want to delete?\n>>> "))
        p.delc(deltarget)

    def g6(self):
        edittarget = str(input("Who do you want to edit?\n>>> "))
        newval = input("New value?\n>>> ")
        p.editn(edittarget,newval)

    def g7(self):
        ans = str(input("Are you sure you want to exit?:\n>>>"))
        if ans == 'y':
            ans2 = str(input("Save?:\n>>>"))
            if ans2 == 'y':
                with open("persist.p", "wb") as f:
                    pickle.dump(p.plist, f)
                    print("Saved!")
                exit()
            else:
                print("Exiting without saving")
                exit()
        else:
            pass

    def g8(self):
        edittarget = str(input("Who do you want to edit?\n>>> "))
        newval = input("New value?\n>>> ")
        p.editmail(edittarget,newval)

class pers:
    def __init__(self,name,num,mob = None, work = None, email = None):
        self.name = name
        self.num = num
        self.mob = mob
        self.work = work
        self.email = email

regexon = 1 #disable for quicker testing

if __name__=="__main__":
    
    

    p = pbook()
    #p.plist = pickle.load( open( "persist.p", "rb" ) )
    with open("persist.p", "rb") as f:
        p.plist = pickle.load(f)

    #p.list()
    g = GUI()
    g.g0()
            
            