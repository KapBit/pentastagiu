import sys

freq = {}
vowels = ("a", "e", "i" ,"o", "u")
l = sys.argv[1:]

for word in l:
    wvowel = []
    for letter in list(word):
        if letter not in freq:
            freq[letter] = 0
        freq[letter] += 1
    if letter in vowels:
        wvowel.append(letter)
    print("The word {} has the vowels {}".format(word,wvowel))

print(freq)

