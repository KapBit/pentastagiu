import math

r = float(input("Enter the circle's radius: \n>>> "))

A = math.pi*pow(r,2)

print("A = {0:.2f}".format(A))
